import PostBody from "../../ui/components/PostBody/PostBody";

export default function MeuPost() {
    return (
        <>
            <PostBody post={{
                id: "1",
                slug: "classes-no-typescript",
                title: "Classes no TypeScript",
                description:
                    "Neste artigo você aprenderá criar classes, implementar métodos e instanciar objetos no TypeScript usando a tipagem adequadamente.",
                picture:
                    "https://dkrn4sk0rn31v.cloudfront.net/uploads/2021/10/tipagem-no-typescript-400x280.png",
                content: (
                    <>
                        <p>
                            No artigo trabalhando com classes em TypeScript, aprendemos a criar uma classe com suas
                            propriedades e instanciar um objeto, agora vamos ver
                            como controlar o comportamento de acesso dessas
                            propriedades, para isso utilizaremos os modificadores
                            de acesso.
                        </p>

                    </>
                )

            }} />
        </>
    )
}