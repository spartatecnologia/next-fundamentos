import PostBody from "../../ui/components/PostBody/PostBody";

export default function MeuPost() {
    return (
        <>
            <PostBody
                post={{
                    id: "2",
                    slug: "o-que-e-nestjs",
                    title: "O que é NestJS?",
                    description:
                        "Vamos ver neste artigo uma breve descrição sobre o que é o NestJS e como criar uma aplicação base em minutos.",
                    picture:
                        "https://dkrn4sk0rn31v.cloudfront.net/uploads/2021/08/NEST-JS-400x280.png",
                    content: (
                        <>
                            <p>
                                O NestJS é um framework back-end que auxilia o desenvolvimento de
                                aplicações eficientes. escaláveis e confiáveis em cima do Node.js.
                                O NestJS utiliza como padrão TypeScript e possui uma sintaxe parecida
                                com Angular. Falando nisso, o NestJS também utiliza o Express “por
                                baixo dos panos”.
                            </p>
                        </>
                    )
                }}
            />
        </>
    )
}