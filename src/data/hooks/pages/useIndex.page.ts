import { BlogPost } from "../../@types/BlogPostInterface";

export default function useIndex() {
  const posts: BlogPost[] = [
    {
      id: "1",
      slug: "classes-no-typescript",
      title: "Classes no TypeScript",
      description:
        "Neste artigo você aprenderá criar classes, implementar métodos e instanciar objetos no TypeScript usando a tipagem adequadamente.",
      picture:
        "https://dkrn4sk0rn31v.cloudfront.net/uploads/2021/10/tipagem-no-typescript-400x280.png",
    },
    {
      id: "2",
      slug: "o-que-e-nestjs",
      title: "O que é NestJS?",
      description:
        "Vamos ver neste artigo uma breve descrição sobre o que é o NestJS e como criar uma aplicação base em minutos.",
      picture:
        "https://dkrn4sk0rn31v.cloudfront.net/uploads/2021/08/NEST-JS-400x280.png",
    },
  ];

  return {
    posts,
  };
}
